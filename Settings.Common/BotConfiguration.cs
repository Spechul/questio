﻿namespace Questio.Configuration
{
    public class BotConfiguration
    {
        public string Nick { get; set; }
        public string ApiKeyFile { get; set; }
        public string Channel { get; set; }
    }
}