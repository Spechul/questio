﻿using System.Collections.Generic;

namespace Questio.Configuration
{
    public class WriteToSection
    {
        public string Name { get; set; }
        public Dictionary<string, string> Args { get; set; } = new Dictionary<string, string>();
    }
}