﻿namespace Questio.Configuration
{
    public class LoggingConfiguration
    {
        public SerilogConfiguration Serilog { get; set; }
    }
}