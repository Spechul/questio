﻿using System.Collections.Generic;

namespace Questio.Configuration
{
    public class SerilogConfiguration
    {
        public List<string> Using { get; set; } = new List<string>();
        public string MinimumLevel { get; set; } = "Information";
        public List<WriteToSection> WriteTo { get; set; } = new List<WriteToSection>();
    }
}