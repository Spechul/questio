﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Questio.Configuration;

namespace Settings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter bot's nickname: ");
            var nick = Console.ReadLine();
            Console.Write("Enter api key file name: ");
            var apiKeyFile = Console.ReadLine();
            Console.Write("Enter channel to connect to: ");
            var channel = Console.ReadLine();
            var botConfig = new BotConfiguration()
            {
                Nick = nick,
                ApiKeyFile = apiKeyFile,
                Channel = channel
            };
            var text = JsonConvert.SerializeObject(botConfig, Formatting.Indented);
            var botConfigFileName = "Configuration.json";
            File.WriteAllText(botConfigFileName, text);
            Console.WriteLine($"{botConfigFileName} is generated, check out working directory");

            Console.Write("Do you want to generate SerilogConfig? (y/n): ");
            var wantGenerateSerilog = Console.ReadLine();
            if (wantGenerateSerilog!.ToLower() == "n")
                return;

            Console.WriteLine("Specify namespaces (separate with space), for example: Serilog.Sinks.Console Serilog.Sinks.File Serilog.Sinks.Seq");
            var @using = Console.ReadLine()!.Split();

            var loggingConfiguration = new LoggingConfiguration
            {
                Serilog = new SerilogConfiguration
                {
                    Using = new List<string>(@using)
                }
            };

            while (true)
            {
                Console.Write("Do you want to add new section? (y/n): ");
                var wantAddSection = Console.ReadLine();
                if (wantAddSection!.ToLower() == "n")
                    break;

                var section = new WriteToSection();
                Console.Write("Name: ");
                section.Name = Console.ReadLine();

                Console.WriteLine("Args (in format \"key:value key:value\")");
                var sinkArgs = Console.ReadLine()!.Split();
                foreach (var arg in sinkArgs)
                {
                    var pair = arg.Split(':');
                    section.Args[pair[0]] = pair[1];
                }

                loggingConfiguration.Serilog.WriteTo.Add(section);
            }

            var loggingText = JsonConvert.SerializeObject(loggingConfiguration, Formatting.Indented);
            var loggingFileName = "SerilogConfiguration.json";
            File.WriteAllText(loggingFileName, loggingText);
            Console.WriteLine($"{loggingFileName} generated");
        }
    }
}
