﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Questio.Configuration;
using Serilog.Core;
using Timer = System.Timers.Timer;

namespace Questio
{
    public class Bot : IDisposable
    {
        private string _ip = "irc.chat.twitch.tv";
        private int _port = 6697;
        private string _nick;
        private readonly Logger _logger;
        private string _password;
        private string _channel;

        private TcpClient _tcpClient;
        private SslStream _sslStream;
        private StreamReader _streamReader;
        private StreamWriter _streamWriter;

        private Timer _serviceInformationTimer = new Timer();
        private double _serviceInformationInterval = 30 * 1000;

        private ConcurrentDictionary<string, int> _userAttempts = new ConcurrentDictionary<string, int>();
        private int _attemptsLimit = 3;
        private CancellationTokenSource _isWorking = new CancellationTokenSource();

        public Bot(BotConfiguration configuration, Logger logger)
        {
            _channel = configuration.Channel;
            _nick = configuration.Nick;
            _logger = logger;
            _password = File.ReadAllText(configuration.ApiKeyFile);
            _tcpClient = new TcpClient();
            _serviceInformationTimer.Elapsed += async (sender, args) =>
            {
                if (_isWorking.Token.IsCancellationRequested)
                {
                    _serviceInformationTimer.Stop();
                }
                _serviceInformationTimer.Interval = _serviceInformationInterval;
                await _streamWriter.WriteLineAsync(
                    $"PRIVMSG #{_channel} : Questio Bot operational. use \"!quo\" to log your question");
            };
        }

        public async Task ConnectAsync()
        {
            await _tcpClient.ConnectAsync(_ip, _port);
            _sslStream = new SslStream(
                _tcpClient.GetStream(),
                false,
                (sender, certificate, chain, errors) => errors == SslPolicyErrors.None,
                null
            );
            await _sslStream.AuthenticateAsClientAsync(_ip);

            _streamReader = new StreamReader(_sslStream);
            _streamWriter = new StreamWriter(_sslStream) { NewLine = "\r\n", AutoFlush = true };

            await _streamWriter.WriteLineAsync($"PASS {_password}");
            await _streamWriter.WriteLineAsync($"NICK {_nick}");
            await _streamWriter.WriteLineAsync($"JOIN #{_channel}");
        }

        public async Task StartAsync()
        {
            //_serviceInformationTimer.Start();
            while (!_isWorking.Token.IsCancellationRequested)
            {
                var line = await _streamReader.ReadLineAsync();
                if (line == null)
                    continue;

                Console.WriteLine(line);
                await ProcessLine(line);
            }
        }

        private async Task ProcessLine(string line)
        {
            var words = line.Split(' ');

            var secondColonPosition = line.IndexOf(":", 1);
            var message = line.Substring(secondColonPosition + 1);

            if (line.StartsWith("PING"))
            {
                await _streamWriter.WriteLineAsync($"PONG {words[1]}"); // response with address
                return;
            }

            var command = message.Split(' ')[0];
            switch (command)
            {
                case "!help":
                {
                    var senderUser = words[0].Split('!')[0].TrimStart(':'); // parse username
                    await _streamWriter.WriteLineAsync(
                        $"PRIVMSG #{_channel} : @{senderUser} available commands: \"!help\" - show this message \"!quo\" - ask a question");
                    break;
                }
                case "!quo":
                {
                    var senderUser = words[0].Split('!')[0].TrimStart(':');
                    if (!_userAttempts.ContainsKey(senderUser))
                        _userAttempts[senderUser] = 0;
                    if (_userAttempts[senderUser] >= _attemptsLimit)
                    {
                        await _streamWriter.WriteLineAsync(
                            $"PRIVMSG #{_channel} : @{senderUser} sorry, you cannot ask more questions this stream");
                        break;
                    }

                    var question = message.Substring(5); // remove ":!quo "
                    _logger.Information("{sender} asks: {question}", senderUser, question);

                    _userAttempts[senderUser]++;
                    var remainingAttempts = _attemptsLimit - _userAttempts[senderUser];

                    await _streamWriter.WriteLineAsync(
                        $"PRIVMSG #{_channel} : @{senderUser} your question has been recorded, you have {remainingAttempts} remaining question(s) for this stream");
                    break;
                }
            }
        }

        public void Dispose()
        {
            _isWorking.Cancel();
            _serviceInformationTimer.Dispose();
            _streamReader.Dispose();
            _streamWriter.Dispose();
            _sslStream.Dispose();
            _tcpClient.Dispose();
        }
    }
}