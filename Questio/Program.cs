﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Questio.Configuration;
using Serilog;

namespace Questio
{
    class Program
    {
        static async Task Main(string[] args)
        {
            BotConfiguration config;
            using (var file = File.OpenText("Configuration/Configuration.json"))
            {
                var serializer = new JsonSerializer();
                config = (BotConfiguration) serializer.Deserialize(file, typeof(BotConfiguration));
            }

            var loggingConfiguration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("Configuration/SerilogConfiguration.json")
                .Build();
            
            using var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(loggingConfiguration)
                .CreateLogger();

            using var bot = new Bot(config, logger);
            await bot.ConnectAsync();
            bot.StartAsync();
            await Task.Delay(-1);
            //await streamWriter.WriteLineAsync($"PRIVMSG #{channel} :dorova");
        }
    }
}
